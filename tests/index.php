<?php
require_once 'include.php';
?>

<h1>Examples</h1>

<h2>Text &amp Rectangle Example: Business Card</h2>
<?php
require_once dirname(__DIR__).'/examples/business-card.php';
?>

<h2>Text Example: </h2>
<?php
require_once dirname(__DIR__).'/examples/text.php';
?>

<h2>Circle Example: Red Background with Yellow Circle</h2>
<?php
require_once dirname(__DIR__).'/examples/red-background-with-yellow-circle.php';
?>