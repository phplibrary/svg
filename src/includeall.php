<?php
require_once dirname(__FILE__).'/Element.php';
require_once dirname(__FILE__).'/Document.php';
require_once dirname(__FILE__).'/Circle.php';
require_once dirname(__FILE__).'/Ellipse.php';
require_once dirname(__FILE__).'/Path.php';
require_once dirname(__FILE__).'/Polygon.php';
require_once dirname(__FILE__).'/Polyline.php';
require_once dirname(__FILE__).'/Rectangle.php';
require_once dirname(__FILE__).'/Text.php';
require_once dirname(__FILE__).'/TSpan.php';

