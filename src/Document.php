<?php

// ========================================================================= //
// SINEVIA PUBLIC                                        http://sinevia.com  //
// ------------------------------------------------------------------------- //
// COPYRIGHT (c) 2016 Sinevia Ltd                        All rights resrved! //
// ------------------------------------------------------------------------- //
// LICENCE: All information contained herein is, and remains, property of    //
// Sinevia Ltd at all times.  Any intellectual and technical concepts        //
// are proprietary to Sinevia Ltd and may be covered by existing patents,    //
// patents in process, and are protected by trade secret or copyright law.   //
// Dissemination or reproduction of this information is strictly forbidden   //
// unless prior written permission is obtained from Sinevia Ltd per domain.  //
//===========================================================================//

namespace Sinevia\Svg;

//============================= START OF CLASS ==============================//
// CLASS: Document                                                            //
//===========================================================================//
/**
 * The Webpage class is the main outer container that holds all the Widgets.
 * It also provides handy interface for specifying webpage details, like
 * placing a favicon, enable/disable scrolling etc.
 * <code>
 * $webpage = new Webpage();
 * $webpage->child("The simplest webpage possible!");
 * $webpage->display();
 * </code>
 */
class Document extends Element {

    protected $title;

    /**
     * The constructor of this Webpage.
     * @construct
     */
    function __construct() {
        parent::__construct();
        /* Encoding - UTF-8 default */
        $this->setEncoding("UTF-8");
        /* Language - EN default */
        //$this->setLanguage("en");
        $this->setAttribute('version', '1.1');
        $this->setAttribute('xmlns', 'http://www.w3.org/2000/svg');
    }

    /**
     * Returns the encoding chrset of the webpage.
     * @return String The encoding as String (null, if not set)
     */
    public function getEncoding() {
        return $this->getProperty('encoding');
    }

    /**
     * Sets the encoding charset of the webpage.
     * @return Webpage an instance of this Webpage
     * @param String the name of the encoding charset (i.e. utf-8)
     */
    public function setEncoding($encoding) {
        $this->setProperty('encoding', strtolower($encoding));
        return $this;
    }

    /**
     * Displays the Webpage to the screen.
     * @param compressed compresses the output, removing the new lines and indent
     * @return void
     */
    function display($compressed = true) {
        if ($this->getOutput() == "xhtml") {
            echo($this->toXhtml($compressed));
            exit;
        } elseif ($this->getOutput() == "html") {
            echo($this->toHtml($compressed));
            exit;
        } else {
            trigger_error('ERROR: In class ' . get_class($this) . ' in method display($compressed): Unknow output MUST BE html or xhtml - <b style="color:red">' . $this->output() . ' given!', E_USER_ERROR);
        }
    }

    /**
     * Returns the XHTML representation of this Webpage with its children.
     * @param compressed compresses the XHTML, removing the new lines and indent
     * @param level the level of this widget
     * @return String xhtml string
     */
    function toXml($compressed = true, $level = 0) {
        if ($compressed == false) {
            $nl = "\n";
            $tab = "    ";
            $indent = str_pad("", ($level * 4));
        } else {
            $nl = "";
            $tab = "";
            $indent = "";
        }
        if ($this->getProperty('title') == null) {
            $this->setProperty('title', "Undefined");
        }
        /* Encoding - UTF-8 default */
        $encoding = $this->getEncoding();
        if ($encoding == null) {
            $encoding = "UTF-8";
        }
        $xml = '<?xml version="1.0" standalone="no" encoding="' . $encoding . '"?>' . $nl;
        $xml .= '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">' . $nl;
        $xml .= '<svg' . $this->attributesToHtml() . $this->cssToHtml() . '>' . $nl;
        if (count($this->children) < 1) {
            $this->children[] = '<p>&nbsp;</p>'; // Empty space to validate the empty page
        }
        foreach ($this->children as $child) {
            if (is_object($child) && is_subclass_of($child, "Sinevia\Svg\Element")) {
                $xml .= $child->toXml($compressed, $level + 1) . $nl;
            } else {
                $xml .= $indent . $tab . $child . $nl;
            }
        }
        $xml .= '</svg>';
        return $xml;
    }

}
