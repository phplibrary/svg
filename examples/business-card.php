<?php

require_once dirname(__DIR__) . '/src/Sinevia/Svg/includeall.php';

$svg = (new Sinevia\Svg\Document)
        ->setWidth("320")
        ->setHeight("200")
        ->setCss("background", "cornsilk");


$photo = (new \Sinevia\Svg\Rectangle())
        ->setX(20)->setY(20)
        ->setWidth(100)->setHeight(100)
        ->setFill("yellow")
        ->setStrokeColor('silver')
        ->setStrokeWidth(1)
        ->setParent($svg);

$name = (new \Sinevia\Svg\Text())
        ->setX(130)->setY(40)
        ->setWidth(30)->setHeight(30)
        ->setFill("#333")
        ->addChild("FIRSTNAME LASTNAME")
        ->setParent($svg);


$title = (new \Sinevia\Svg\Text())
        ->setX(130)->setY(80)
        ->setWidth(30)->setHeight(30)
        ->setFill("#333")
        ->addChild("WORK TITLE")
        ->setParent($svg);


$email = new \Sinevia\Svg\Text();
$email->setX(130)->setY(140)
        ->setWidth(30)->setHeight(30)
        ->setFill("#666")
        ->setStrokeWidth(1)
        ->addChild("E-mail: info@sinevia.com")
        ->setParent($svg);


$phone = new \Sinevia\Svg\Text();
$phone->setX(130)->setY(160)
        ->setWidth(30)->setHeight(30)
        ->setFill("#666")
        ->setStrokeWidth(1)
        ->addChild("Phone: +44(0)123-456-789")
        ->setParent($svg);


$fax = new \Sinevia\Svg\Text();
$fax->setX(130)->setY(180)
        ->setWidth(30)->setHeight(30)
        ->setFill("#666")
        ->setStrokeWidth(1)
        ->addChild("Fax: +44(0)123-456-789")
        ->setParent($svg);


echo $svg->toXml();

?>