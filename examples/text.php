<?php

require_once dirname(__DIR__).'/src/Sinevia/Svg/includeall.php';

$svg = new Sinevia\Svg\Document;
$svg->setAttribute("width", "260");
$svg->setAttribute("height", "260");
$svg->setCss("background", "cornsilk");

$text = new \Sinevia\Svg\Text();
$text->setX(50)->setY(80)
        ->setWidth(30)->setHeight(30)
        ->setFill("yellow")
        ->setStrokeColor('silver')
        ->setStrokeWidth(1)
        ->addChild("LINE 1")
        ->setParent($svg);

$text = new \Sinevia\Svg\Text();
$text->setX(50)->setY(140)
        ->setWidth(30)->setHeight(30)
        ->setFill("yellow")
        ->setStrokeColor('aqua')
        ->setStrokeWidth(1)
        ->addChild("LINE 2")
        ->setParent($svg);


$text = new \Sinevia\Svg\Text();
$text->setX(50)->setY(200)
        ->setWidth(30)->setHeight(30)
        ->setFill("yellow")
        ->setStrokeColor('lime')
        ->setStrokeWidth(1)
        ->addChild("LINE 3")
        ->setParent($svg);



 //cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" /


echo $svg->toXml();

