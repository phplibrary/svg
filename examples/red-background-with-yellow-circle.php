<?php

require_once dirname(__DIR__).'/src/Sinevia/Svg/includeall.php';

$svg = new Sinevia\Svg\Document;
$svg->setAttribute("width", "160");
$svg->setAttribute("height", "160");
$svg->setCss("background", "red");

$circle = new \Sinevia\Svg\Circle();
$circle->setX(80)->setY(80)
        ->setWidth(30)->setHeight(30)
        ->setRadius(60)
        ->setFill("yellow")
        ->setStrokeColor('silver')
        ->setStrokeWidth(3)
        ->setParent($svg);


$circle = new \Sinevia\Svg\Circle();
$circle->setX(80)->setY(80)
        ->setWidth(30)->setHeight(30)
        ->setRadius(30)
        ->setFill("green")
        ->setStrokeColor('silver')
        ->setStrokeWidth(3)
        ->setParent($svg);



 //cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" /


echo $svg->toXml();

