
<style>
    svg {
    width: 6rem;
    height: 6rem;
}

div {
    display: inline-block;
    margin: 1rem;
    padding: .5rem;
}

.demo1 .top {
    fill: #3fa535;
}

.demo1 .right {
    fill: #3fd935;
}

.demo1 .bottom {
    fill: #004616;
}

.demo1 .left {
    fill: #257735;
}

.demo2 .top {
    fill: #356BA5;
}

.demo2 .right {
    fill: #357FD9;
}

.demo2 .bottom {
    fill: #0E1246;
}

.demo2 .left {
    fill: #253577;
}
</style>
<svg>
	<use class="top" xlink:href="#top"></use>
	<use class="right" xlink:href="#right"></use>
	<use class="bottom" xlink:href="#bottom"></use>
	<use class="left" xlink:href="#left"></use>
</svg>
<svg xmlns="http://www.w3.org/2000/svg">
	<symbol id="top" viewBox="0 0 54 54">
		<polygon points="54 0 0 0 27 27 54 0"></polygon>
	</symbol>
	<symbol id="right" viewBox="0 0 54 54">
		<polygon points="54 54 54 0 27 27 54 54"></polygon>
	</symbol>
	<symbol id="bottom" viewBox="0 0 54 54">
		<polygon points="0 54 54 54 27 27 0 54"></polygon>
	</symbol>
	<symbol id="left" viewBox="0 0 54 54">
		<polygon points="0 0 0 54 27 27 0 0"></polygon>
	</symbol>
</svg>

