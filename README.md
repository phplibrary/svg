#SINEVIA SVG

A general purpose library for working with SVG

### Installation with Composer

```
#!json


    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/phplibrary/svg.git"
        }
    ],
    "require": {
        "sinevia/svg": "dev-master"
    }
```


### Manual Installation

Download and install the source folder into a directory of your choosing. Include the autoinclude.php file from the source folder.


```
#!php

require_once '/src/includeall.php';
```

## Usage ##



```
#!php

<?php

require_once dirname(__DIR__).'/src/Sinevia/Svg/includeall.php';

$svg = new Sinevia\Svg\Document;
$svg->setAttribute("width", "160");
$svg->setAttribute("height", "160");
$svg->setCss("background", "red");

$circle = new \Sinevia\Svg\Circle();
$circle->setX(80)->setY(80)
        ->setWidth(30)->setHeight(30)
        ->setRadius(60)
        ->setFill("yellow")
        ->setStrokeColor('silver')
        ->setStrokeWidth(3)
        ->setParent($svg);


echo $svg->toXml();
```
